import { MuseumslidesPage } from './app.po';

describe('museumslides App', function() {
  let page: MuseumslidesPage;

  beforeEach(() => {
    page = new MuseumslidesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
