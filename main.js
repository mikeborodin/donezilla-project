const {
  app,
  BrowserWindow,
  ipcMain,
  globalShortcut
} = require('electron');
const {
  exec
} = require('child_process');

let devEnv = true

let mainScreen = null;

app.on('ready', function () {

  mainScreen = new BrowserWindow({
    width: 500,
    height: 800,
    title: "DoneZilla",
    x: 50,
    y:  50,
    webPreferences: {
      webSecurity: false
    },
  

  });
  mainScreen.setMenu(null);

  //DEV
  mainScreen.maximize();
  if (devEnv) {
    mainScreen.webContents.openDevTools();
    let host = 'localhost';
    mainScreen.loadURL(`http://${host}:7878`);
  } else {
    mainScreen.loadURL("file://" + __dirname + "/dist/index.html"); //prod
  }

  // Remove mainScreendow once app is closed
  mainScreen.on('closed', function () {
    mainScreen = null;
  });

  /* ADMIN */
  const retAdmin = globalShortcut.register('Ctrl+Q', () => {
    console.log('Ctrl+Q (Admin) is pressed');
    mainScreen.webContents.send('on-admin-hotkey');
  });
  if (!retAdmin) {
    console.log('registration failed')
  }
  console.log(globalShortcut.isRegistered('Ctrl+A'));
});

/* app.on('activate', () => { if (mainScreen === null) { createMainWindow() } });*/
app.on('will-quit', () => {
  globalShortcut.unregister('Ctrl+X')
  globalShortcut.unregisterAll()
})


app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});