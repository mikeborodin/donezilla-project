import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatTable, MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
}
  from '@angular/material';
import { EmployeeService } from '../../data/employee.service';
import { AuthService } from '../../data/auth.service';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) matTable: MatTable<Employee>;

  displayedColumns = ['Photo', 'Name', 'Email', 'Access_Level'];
  employees: Array<Employee> = []
  dataSource = new MatTableDataSource<Employee>(this.employees);
  constructor(
    private es: EmployeeService,
    private auth: AuthService,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    if (this.auth.currentUser.Access_Level == 1) {
      this.displayedColumns = ['Photo', 'Name', 'Email', 'Access_Level', 'actions']
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.updateEmployees()
  }

  updateEmployees() {
    this.es.getAllEmployees().then((res) => {
      this.dataSource = new MatTableDataSource<Employee>(res)
      this.matTable.renderRows()
      this.paginator._changePageSize(this.paginator.pageSize);
    }).catch(console.error)
  }

  edit(emp) {
    let dialogRef = this.dialog.open(EditDialogComponent, {
      width: '600px',
      data: { employee: emp }
    });
    dialogRef.afterClosed().subscribe(e => {
      console.log('The dialog was closed');
      e.save().then(() => {
        alert(`Updated ${e.Name}'s account`)
      }).catch(console.error)
    });
  }

  deleteAccount(emp) {
    this.es.deleteAccount(emp.Employee_Id).then(() => {
      this.updateEmployees()
    }).catch(console.log
    )
  }

  addEmployee() {
    let dialogRef = this.dialog.open(EditDialogComponent, {
      width: '600px',
      data: { employee: this.es.createEmptyEmployee() }
    });
    dialogRef.afterClosed().subscribe(emp => {
      if (emp) {
        console.log('The dialog was closed');
        emp.save().then(() => {
          console.log(`Creating login`, emp.identity());
          this.auth.createPassword(emp.identity(), '1111').then(() => {
            alert(`Created ${emp.Name}'s account!\nPassword: 1111`)
            this.updateEmployees()
          }).catch(console.log)
        }).catch(console.error)
      }
    });
  }

}

export interface Employee {
  Name: string;
  Photo: number;
  Email: number;
  Access_Level: number;
}

