import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AuthService } from '../../../data/auth.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {

  employee
  isAdmin: boolean = false
  constructor(
    private auth: AuthService,
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.employee = data.employee
    this.isAdmin = auth.currentUser.Access_Level == 1
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close(this.employee);
  }

  onAccessChange(event){
    this.employee.Access_Level = (event.checked==1)
  }
}
