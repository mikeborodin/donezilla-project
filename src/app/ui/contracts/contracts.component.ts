import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../../data/customer.service';
import { MatPaginator, MatTable, MatTableDataSource } from '@angular/material';
import { ProjectService } from '../../data/project.service';
import { DbService } from '../../data/db.service';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) matTable: MatTable<Contract>;

  displayedColumns = ['Title', 'Budget', 'Document_Link', 'actions']

  contracts: Array<Contract> = []
  projects = new Map<number, string>()
  projectsArr = []

  customers = new Map<number, string>()
  customersArr = []

  contract: any = {
    Project_Id: 0
  }
  dataSource = new MatTableDataSource<Contract>(this.contracts);


  constructor(private cs: CustomerService, private ps: ProjectService, private dbs: DbService) {

  }

  ngOnInit() {

    this.list()

    this.ps.getAllProjects().then((res) => {
      this.projectsArr = []
      res.forEach((r) => {
        this.projectsArr.push(r)
        this.projects[r.Project_Id] = r.Title
      })

      console.log(this.projects);
      console.log(this.projectsArr);

    })


    this.cs.getAllCustomers().then((res) => {
      this.customersArr = []
      res.forEach((r) => {
        this.customersArr.push(r)
        this.customers[r.Customer_Id] = r.Name
      })

      console.log(this.projects);
      console.log(this.projectsArr);

    })
  }

  list() {
    this.cs.getAllContracts().then((res) => {
      this.dataSource = new MatTableDataSource<Contract>(res)
      this.matTable.renderRows()
      this.paginator._changePageSize(this.paginator.pageSize);
    }).catch(console.error)
  }

  edit(con) {
    this.contractOpen = true
    this.contract = con
  }

  delete(con) {
    console.log(con);
    
    this.cs.deleteContract(con.Contract_Id)
  }

  contractOpen = false

  add() {
    this.contractOpen = true
  }

  save() {
    console.log(this.contract);
    this.dbs.Contract(this.contract).save()
  }
}

export interface Contract {
  Project_Title: string;
  Document_Link: string;
  Budget: number;
}

