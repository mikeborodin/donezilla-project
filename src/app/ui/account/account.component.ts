import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../data/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  changePassVisible: boolean = false
  constructor(private auth: AuthService) {

    this.currentUser = auth.currentUser


  }

  currentUser: any
  newPassword: string = ''
  saveAccountBtn = 'ЗБЕРЕГТИ'
  newPassBtn = 'ПІДТВЕРДИТИ'
  levels = []
  skills = []
  mySkills = []

  newSkillId
  newLevelId

  ngOnInit() {

    this.auth.getAllSkills().then((res) => {
      this.skills = res
      console.log(res);
    }).catch(console.log)


    this.auth.getLevels().then((res) => {
      this.levels = res

      console.log(res);
    }).catch(console.log)

    this.updateMySkills()
    console.log(`On Init`);

  }

  changePass() {
    if (this.newPassword.length >= 6) {
      this.auth.changePassword(this.newPassword).then(() => {
        this.newPassBtn = "ЗМІНЕНО"
        setTimeout(() => {
          this.newPassBtn = "ПІДТВЕРДИТИ"
        }, 5000)
      }).catch(alert)
    } else {
      alert('Пароль мень нід 6 букв')
    }

  }

  submit() {
    this.auth.updateUserAccount(this.currentUser).then(() => {
      this.saveAccountBtn = "ЗБЕРЕЖЕНО"
      setTimeout(() => {
        this.saveAccountBtn = "ЗБЕРЕГТИ"
      }, 5000)
    }).catch(alert)
  }

  addSkill(newSkillId, newLevelId) {
    this.saveAccountBtn = "ДОДАЄМО..."
    this.auth.addSkill(newSkillId, newLevelId).then(() => {
      this.saveAccountBtn = "ДОДАТИ"
      this.updateMySkills()
    }).catch(() => {
      alert('Ви вже маєте дану навичку')
    })
  }

  removeMySkill(mySkill) {
    this.auth.rmSkill(mySkill.Skill_Id).catch((err) => {
      alert('Помилка видаляння')
    }).then(()=>{
      this.updateMySkills()
    })
  }

  updateMySkills() {
    this.auth.getMySkills().then((res) => {
      this.mySkills = []
      res.forEach((e)=>{
        this.mySkills.push(e)
      })
      console.log(res);
    }).catch(console.log)
  }
}
