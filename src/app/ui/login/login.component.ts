import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../data/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string = 'mike@done.com'
  password: string = 'zxcvbnm'
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
   // this.submit()
  }

  submit() {
    this.auth.login(this.username, this.password).then(() => {
      this.router.navigate(['dashboard'])
    }).catch((err) => {
      alert(err)
    })
  }
}
