import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../data/auth.service';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { ProjectService } from '../../data/project.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user
  title: string = "Home"
  customersVisible = true
  contractsVisible = true
  emplVisible = true

  constructor(private router: Router,
    public auth: AuthService,
    private ps:ProjectService,
    private activatedRoute: ActivatedRoute) {

    this.user = auth.currentUser

    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => {
        this.title = event['title']
      });

  }

  ngOnInit() {
    if (!this.auth.currentUser) {
      this.router.navigate(['login'])
    }
    this.emplVisible = this.user && this.user.Access_Level==1
    this.customersVisible = this.user && this.user.Access_Level==1
    this.contractsVisible = this.user && this.user.Access_Level==1

  }


  logout() {
    this.auth.logout()
    this.router.navigate(['login'])
  }

  addProject(){
    this.ps.addProject()
  }
}
