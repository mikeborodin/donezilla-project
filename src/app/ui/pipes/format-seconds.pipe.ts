import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatSeconds'
})
export class FormatSecondsPipe implements PipeTransform {

  transform(seconds: number, args?: any): any {
    return `${('0' + Math.floor(seconds / 60)).slice(-2)}:${('0'+Math.floor(seconds % 60)).slice(-2)}`;
  }

}
