import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../../data/auth.service';
import { MatTableDataSource } from '@angular/material';

import {
  MatPaginator,
  MatTable, MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
}
  from '@angular/material';
import { CustomerService } from '../../data/customer.service';
import { CustomerEditDialogComponent } from './customer-edit-dialog/customer-edit-dialog.component';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) matTable: MatTable<Customer>;
  customers: Array<Customer> = []
  dataSource = new MatTableDataSource<Customer>(this.customers);

  displayedColumns = ['Name', 'Email', 'Phone', 'actions']


  constructor(
    private cs: CustomerService,
    private auth: AuthService,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog) { }

  ngOnInit() {
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.updateCustomers()
  }


  updateCustomers(): any {
    this.cs.getAllCustomers().then((res) => {
      this.dataSource = new MatTableDataSource<Customer>(res)
      this.matTable.renderRows()
      this.paginator._changePageSize(this.paginator.pageSize);
    }).catch(console.error)
  }


  edit(cus) {
    let dialogRef = this.dialog.open(CustomerEditDialogComponent, {
      width: '600px',
      data: { customer: cus }
    });
    dialogRef.afterClosed().subscribe(e => {
      if (e) {
        console.log('The dialog was closed');
        e.save().then(() => {
        }).catch(console.error)
      }
    });
  }

  deleteCustomer(cus) {
    this.cs.deleteCustomer(cus.Customer_Id).then(() => {
      this.updateCustomers()
    }).catch(console.log
    )
  }

  addCustomer() {
    let dialogRef = this.dialog.open(CustomerEditDialogComponent, {
      width: '600px',
      data: { customer: this.cs.createEmptyCustomer() }
    });
    dialogRef.afterClosed().subscribe(cus => {
      if (cus) {
        console.log('The dialog was closed');
        cus.save().then(() => {
          console.log(`Saved ok`, cus.identity());
        }).catch(console.error)
      }
    });
  }
}


export interface Customer {
  Name: string;
  Email: number;
  Photo: number;
}
