import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTable, MatTableDataSource } from '@angular/material';
import { TaskService } from '../../data/task.service';
import { DbService } from '../../data/db.service';
import { EmployeeService } from '../../data/employee.service';
import { ProjectService } from '../../data/project.service';
import { TeamService } from '../../data/team.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('matTable') matTable: MatTable<Task>;

  @ViewChild('calPaginator') calPaginator: MatPaginator;
  @ViewChild('tableCal') tableCal: MatTable<any>;


  displayedColumns = ['Title', 'Start_Date', 'End_Date', 'Employee_Id', 'Status', 'actions']
  // calColumns = ['Title', 'Start_Date', 'End_Date', 'Employee_Id', 'Status', 'actions']
  calColumns = ['Start_Date', 'End_Date', 'Employee_Id', 'actions']

  employees = new Map<number, string>()
  employeesArr = []
  statuses = []

  taskOpen = false
  task: any = {
  }
  team: any



  projectId: number
  tasks = []

  cals = []
  cal: any = {}
  calOpen = false

  projectsArr = []
  dataSource = new MatTableDataSource<Task>(this.tasks);
  calDataSource = new MatTableDataSource<any>(this.cals);

  constructor(
    private ts: TaskService,
    private tms: TeamService,
    private es: EmployeeService,
    private ps: ProjectService,
    private dbs: DbService) { }

  ngOnInit() {

    this.es.getAllEmployees().then((res) => {
      res.forEach((r) => {
        this.employees[r.Employee_Id] = r
        this.employeesArr.push(r)
      })
    }).catch(console.log)

    this.ps.getAllProjects().then((res) => {
      res.forEach((r) => {
        this.projectsArr.push(r)
      })
    }).catch(console.log)

    this.statuses.push({ val: 0, title: `Не почато` })
    this.statuses.push({ val: 1, title: 'В процесі' })
    this.statuses.push({ val: 2, title: 'Завершено' })
  }

  emplId:number
  updateEmployeeTasks(eid) {
    console.log(eid);
    
  this.ts.getTasksByEmployee(this.projectId, eid.value).then((res) => {
      this.tasks = res
      this.dataSource = new MatTableDataSource<Task>(res)
      this.matTable.renderRows()
      this.paginator._changePageSize(this.paginator.pageSize);
    }).catch(console.log)
  }

  //Tasks
  list(e) {
    // console.log(e);
    // console.log(this.projectId);
    this.ts.getProjectTasks(this.projectId).then((res) => {
      this.tasks = res
      console.log(res);
      this.dataSource = new MatTableDataSource<Task>(res)
      this.matTable.renderRows()
      this.paginator._changePageSize(this.paginator.pageSize);
    }).catch(console.error)


    this.tms.getProjectTeam(this.projectId).then((res) => {
      this.team = res
    })
    this.listCals()
  }

  edit(task) {
    this.task = task
    this.openTask()
  }

  openTask() {
    this.taskOpen = true
  }

  close() {
    this.taskOpen = false
  }

  onTaskStartChanged(date) {
    this.task.Start_Date = date.value
  }
  onTaskEndChanged(date) {
    this.task.End_Date = date.value
  }

  saveTask() {
    console.log(`Saving`, this.task);
    this.task.Project_Id = this.projectId
    this.dbs.Task(this.task).upsert(() => {
    }).catch(console.error)
  }

  delete(task) {
    this.ts.deleteTask(task.Task_Id).then(() => {
    }).catch(console.error)
  }

  getTaskStatus(s) {
    return this.ts.getTaskStatus(s)
  }


  // Cals
  listCals() {
    this.ts.getAllCals(this.projectId).then(res => {
      this.cals = res
      this.calDataSource = new MatTableDataSource<Task>(this.cals)
      this.tableCal.renderRows()
      ///this.tableCal._changePageSize(this.tableCal.pageSize);
      console.log(this.cals);
    })
  }


  editCal(cal) {
    this.cal = cal
    this.openCal()
  }

  openCal() {
    this.calOpen = true
  }

  closeCal() {
    this.calOpen = false
  }

  onCalStartChanged(date) {
    this.cal.Date_Start = date.value
  }
  onCalEndChanged(date) {
    this.cal.Date_End = date.value
  }

  saveCal() {
    this.cal.Project_Id = this.projectId
    this.cal.Team_Id = this.team.Team_Id
    console.log(`Saving`, this.cal);

    this.dbs.Load_Calendar(this.cal).upsert(() => {
      console.log(`Saved`, this.cal);

    }).catch(console.error)
  }

  deleteCal(cal) {
    this.ts.deleteCal(cal.Calendar_Id).then(() => {
    }).catch(console.error)
  }




























}

export interface Task {
  Task_Id: string,
  Title: string,
  Description,
  Start_Date: Date,
  End_Date: Date,
  Status: number
}