import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../../data/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../data/auth.service';
import { TeamService } from '../../../data/team.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {


  project: any
  team: any
  requiredSkills = []
  allSkills = []
  saveBtn = 'ЗБЕРЕГТИ'

  newSkillId
  newSkillStart
  newSkillEnd

  constructor(
    private auth: AuthService,
    private router: Router,
    private ps: ProjectService,
    private ts: TeamService,
    private ar: ActivatedRoute) {
  }

  ngOnInit() {

    this.auth.getAllSkills().then((res) => {
      this.allSkills = res
    }).catch(console.log)

    this.ar.params.subscribe(p => {
      this.ps.loadProject(p['id']).then((p) => {
        this.project = p

        this.updateRequiredSkills()
        this.updateAvailableEmployee()
        this.getTeam()
        this.getOfficess()

      }).catch((err) => {
        console.log(err);
      })
    })



  }

  updateRequiredSkills() {
    this.ps.getRequiredSkills(this.project.Project_Id).then((res) => {
      this.requiredSkills = []
      res.forEach(e => {
        this.requiredSkills.push(e)
      })
      console.log(`req sk`, this.requiredSkills);
    }).catch(alert)
  }




  getProjectStatus() {
    return this.ps.getProjectStatus(this.project)
  }

  onStartChanged(date) {
    console.log(date.value);
    this.project.Start_Date = date.value
  }

  onEndChanged(date) {
    console.log(date.value);
    this.project.End_Date = date.value
  }


  //Requirements

  onNewSkillStartChanged(date) {
    this.newSkillStart = date.value
  }



  onNewSkillEndChanged(date) {
    this.newSkillEnd = date.value
  }


  onNewSkillStartChangedItem(rs, date) {
    date.value
    console.log(rs);

  }

  onNewSkillEndChangedItem(rs, date) {
    date.value
  }

  delete() {
    this.ps.removeProject(this.project.Project_Id).then(() => {
      this.router.navigate(['dashboard', 'projects'])
    }).catch(alert)
  }

  save() {
    this.project.save().then(() => {
      console.log(`Saved ok`);
      this.saveBtn = 'ЗБЕРЕЖЕНО'
      setTimeout(() => {
        this.saveBtn = 'ЗБЕРЕГТИ'
      }, 3000)
    }).catch((err) => {
      console.log(err);
    })
  }

  addRequiredSkill() {
    this.ps.addRequiredSkill(this.project.Project_Id, this.newSkillId,
      this.newSkillStart, this.newSkillEnd).then(() => {
        this.updateRequiredSkills()
      }).catch(console.log)
  }
  removeRequiredSkill(sid) {
    console.log(`removeRequiedSkill`, sid);

    return this.ps.removeRequiredSkill(this.project.Project_Id, sid.Skill_Id).then(() => {
      this.updateRequiredSkills()
    }).catch(console.log)
  }

  availableEmployees = []

  updateAvailableEmployee() {
    this.ts.getAvalibleTeamMembers(this.requiredSkills, this.project.Project_Id).then((res) => {
      this.availableEmployees = []
      console.log(`Empl`, res);
      res.forEach((e) => {
        this.availableEmployees.push(e)
      })
    }).catch(console.log);
  }

  //Team
  saveTeam() {
    this.ts.saveTeam(this.team).then(() => {

    }).catch(console.error)
  }

  getTeam() {
    this.ts.getProjectTeam(this.project.Project_Id).then((res) => {
      this.team = res
      this.updateTeamMemebers()
    }).catch(console.error);
  }

  addTeamMember(eid) {
    this.ts.addTeamMember(eid, this.team.Team_Id, this.project.Project_Id).then(() => {
      this.updateTeamMemebers()
    }).catch(console.error);
  }
  removeTeamMember(eid) {
    this.ts.removeTeamMember(eid, this.team.Team_Id)
    this.updateTeamMemebers()
  }
  teamMembers = []

  updateTeamMemebers() {
    this.ts.getTeamMembers(this.team.Team_Id).then((res) => {
      this.teamMembers = res
    }).catch(console.error)
  }

  offices = []
  getOfficess() {
    this.ts.getAllOfficess().then(res => {
      this.offices = res
      console.log(`off`, this.offices);
    })
  }


  ngOnDestroy() {
  }
}
