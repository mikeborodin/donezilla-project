import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../data/project.service';
import { ProjectStatus } from '../../domain/ProjectStatus';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  projects = []
  constructor(private ps: ProjectService, private router: Router) { }

  ngOnInit() {
    this.ps.getAllProjects().then((res) => {
      res.forEach((p) => {
        this.projects.push(p)
      })
    })

  
  }

  getProjectStatus(p) {
    return this.ps.getProjectStatus(p)
  }

  addProject() {
    this.ps.addProject().then((id) => {
      this.router.navigate([`dashboard`,`projects`,`${id}`])
    }).catch(console.error)
  }
}
