import { Component } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from '@angular/router';
import { ElectronService } from 'ngx-electron'

@Component({
  selector: 'app-root', 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    // private ms: MuseumService,
    private es: ElectronService,
    private router: Router) {

    let ipc = this.es.ipcRenderer;
    // ipc.on('on-admin-hotkey', (event, message) => {
    //   console.log('accepted on-admin-hotkey in app');
    // });
  }

  onBackPress(): void {
    this.location.back();
  }
}
