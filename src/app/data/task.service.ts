import { Injectable } from '@angular/core';
import { DbService } from './db.service';
import { TaskStatus } from '../domain/TaskStatus';

@Injectable()
export class TaskService {



  constructor(private dbs: DbService) { }

  getAllTasks(): Promise<Array<any>> {
    let sql = 'SELECT * FROM Task'
    return this.dbs.Task.query(sql)
  }

  getProjectTasks(pid): Promise<Array<any>> {
    let sql = 'SELECT * FROM Task WHERE Project_ID=@pid'
    return this.dbs.Task.query(sql, { pid: pid })
  }




  getTaskStatus(s): string {
    return TaskStatus[s]
  }




  //Cals
  getAllCals(pid): Promise<Array<any>> {
    let sql = 'SELECT * FROM Load_Calendar WHERE Project_Id=@id'
    return this.dbs.Customer.query(sql, { id: pid })
  }
  deleteCal(cid): Promise<any> {
    let sql = 'DELETE FROM Load_Calendar WHERE Calendar_Id=@id'
    return this.dbs.Customer.query(sql, { id: cid })
  }

  // updateCal(cid):Promise<any>{
  //   let sql = 'UPDATE  Load_Calendar SET Date_Start=@start, Date_End=@end, Load_Rate=@rate, Employee_Id=@eid WHERE Calendar_Id=@id'
  //   return this.dbs.Customer.query(sql, { id: cid })
  // }
  deleteTask(tid): Promise<any> {
    let sql = 'DELETE  FROM Task WHERE Task_Id=@id'
    return this.dbs.Customer.query(sql, { id: tid })
  }

  getTasksByEmployee(eid, pid): Promise<any> {
    let sql = 'SELECT * FROM Task WHERE Employee_Id=@id AND Project_Id=@pid'
    return this.dbs.Employee.query(sql, { id: eid, pid: pid })
  }
 
}
