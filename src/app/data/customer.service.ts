import { Injectable } from '@angular/core';
import { DbService } from './db.service';

@Injectable()
export class CustomerService {

  
 
  constructor(private dbs: DbService) { }

  createEmptyCustomer(): any {
    return this.dbs.Customer({
      Name: '',
      Email: '',
      Phone: ''
    })
  }

  getAllCustomers(): Promise<Array<any>> {
    let sql = 'SELECT * FROM Customer'
    return this.dbs.Customer.query(sql)
  }

  getAllContracts(): Promise<Array<any>> {
    let sql = 'SELECT * FROM Contract'
    return this.dbs.Contract.query(sql)
  }

  deleteCustomer(cid): Promise<any> {
    let sql = 'DELETE  FROM Customer WHERE Customer_Id=@cid'
    return this.dbs.Customer.query(sql, { cid: cid })
  }

  deleteContract(cid): Promise<any> {
    let sql = 'DELETE  FROM Contract WHERE Contract_Id=@cid'
    return this.dbs.Customer.query(sql, { cid: cid })
  }
 

}
