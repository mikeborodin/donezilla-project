import { Injectable } from '@angular/core';
import { DbService } from './db.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  constructor(private dbs: DbService, private router: Router) { }

  currentUser

  login(email: string, password: string): Promise<any> {
    return new Promise((ok, err) => {
      let sql = "SELECT * FROM Login WHERE Login_Id=(SELECT Employee_Id FROM Employee WHERE Email=@email) AND Password=@password"
      this.dbs.db.query(sql, { email: email, password: password }).then((res) => {
        if (res.length) {
          this.dbs.Employee.query('SELECT * FROM Employee where Employee_Id=@id', { id: res[0].Login_Id })
            .then((res) => {
              this.currentUser = res[0]
              ok()
            })
        } else {
          err('Email or password is invalid.')
        }
      })
    })
  }

  logout() {
    this.currentUser = null
  }

  updateUserAccount(user) {
    return user.save()
  }

  changePassword(pass: string): Promise<Boolean> {
    return new Promise((ok, err) => {
      if (!this.currentUser) {
        err('User logged out')
      } else {
        this.dbs.db.query('UPDATE  Login SET  Password=@newPass where Login_Id=@id',
          { id: this.currentUser.Employee_Id, newPass: pass })
          .then(() => {
            ok()
          }).catch((e) => {
            console.log(e);
            err('Db error')
          })
      }
    })
  }


  setPassword(eid, pass): Promise<any> {
    return this.dbs.Login({
      Login_Id: eid,
      Password: pass
    }).save()
  }
  createPassword(eid, pass): Promise<any> {
      let sql =  `INSERT INTO Login(Login_Id, [Password]) VALUES (@eid, @pass)`
      return this.dbs.db.query(sql,{eid:eid, pass:pass})
  }


  getMySkills(): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = `SELECT Employee_Id, S.Skill_Id,L.Level_Id, S.Title as Skill_Title,L.Title as Level_Title FROM Employee_Skill
      JOIN Skill S ON Employee_Skill.Skill_Id = S.Skill_Id
      JOIN Level L ON Employee_Skill.Level_Id = L.Level_Id WHERE Employee_Id=@id`
      this.dbs.db.query(sql, { id: this.currentUser.Employee_Id }).then((res) => {
        ok(res)
      }).catch(err)
    })
  }

  getAllSkills(): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Skill'
      this.dbs.db.query(sql).then((res) => {
        ok(res)
      }).catch((err) => {
        console.log(err);
      })
    })
  }
  getLevels(): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Level'
      this.dbs.db.query(sql).then((res) => {
        ok(res)
      }).catch(err)
    })
  }
  addSkill(skillId: number, levelId: number): Promise<any> {
    return new Promise((ok, err) => {
      let sql = 'INSERT INTO Employee_Skill(Employee_Id, Skill_Id, Level_Id) VALUES (@eid,@sid,@lid)'
      this.dbs.db.query(sql,
        {
          eid: this.currentUser.Employee_Id,
          sid: skillId,
          lid: levelId
        }).then((res) => {
          ok(res)
        }).catch(err)
    })
  }

  rmSkill(skillId: number): Promise<any> {
    return new Promise((ok, err) => {
      let sql = 'DELETE FROM Employee_Skill WHERE Employee_Id=@eid AND Skill_Id=@sid'
      this.dbs.db.query(sql,
        {
          eid: this.currentUser.Employee_Id,
          sid: skillId,
        }).then((res) => {
          ok(res)
        }).catch(err)
    })
  }

}
