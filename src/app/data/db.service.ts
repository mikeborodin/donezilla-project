import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { environment } from 'environments/environment';

@Injectable()
export class DbService {
  private

  db: any

  //My ORM
  Employee
  Skill
  Employee_Skill
  Employee_Team
  Team
  Positions

  Load_Calendar
  Project
  Task
  Comment

  Customer
  Contract
  Required_Skills
  Office

  Level
  Login


  constructor(private es: ElectronService, private zone: NgZone) {
    let sworm = this.es.remote.require('sworm')
    this.db = sworm.db(environment.dbOptions);
    this.initModels(this.db)
  }

  initModels(db) {
    this.Employee = db.model({ table: 'Employee', id: 'Employee_Id' })
    this.Skill = db.model({ table: 'Skill', id: 'Skill_Id' })
    this.Employee_Skill = db.model({ table: 'Employee_Skill' })
    this.Employee_Team = db.model({ table: 'Employee_Team' })
    this.Team = db.model({ table: 'Team', id: 'Team_Id' })
    this.Positions = db.model({ table: 'Positions', id: 'Position_Id' })

    this.Load_Calendar = db.model({ table: 'Load_Calendar', id: 'Calendar_Id' })
    this.Project = db.model({ table: 'Project', id: 'Project_Id' })
    this.Task = db.model({ table: 'Task' })
    this.Comment = db.model({ table: 'Comment', id: 'Comment_Id' })

    this.Customer = db.model({ table: 'Customer', id: 'Customer_Id' })
    this.Contract = db.model({ table: 'Contract', id: 'Contract_Id' })
    this.Required_Skills = db.model({ table: 'Required_Skills' })
    this.Office = db.model({ table: 'Office', id: 'Office_Id' })

    this.Level = db.model({ table: 'Level', id: 'Level_Id' })
    this.Login = db.model({ table: 'Login', id: 'Login_Id' })
  }

}
