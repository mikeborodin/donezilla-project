import { Injectable } from '@angular/core';
import { DbService } from './db.service';

@Injectable()
export class TeamService {
  constructor(private dbs: DbService) { }

  getAvalibleTeamMembers(skills, pid): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = `SELECT * FROM Employee WHERE Employee_Id NOT IN (
        SELECT Employee_Id FROM Load_Calendar C WHERE
          C.Date_Start <= (SELECT End_Date FROM Project WHERE C.Project_Id=@pid)
        AND (C.Date_End >= (SELECT Start_Date FROM Project WHERE C.Project_Id=@pid))
      )`
      this.dbs.db.query(sql, { pid: pid }).then((res) => {
        ok(res)
      }).catch(err)
    })
  }

  saveTeam(team) {
    return team.save()
  }

  addTeamMember(eid, tid, pid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `INSERT INTO Employee_Team(Employee_Id, Team_Id, Project_Id)  VALUES (@eid,@tid,@pid)`
      this.dbs.db.query(sql, { eid: eid, tid: tid, pid: pid }).then(() => {
        ok()
      }).catch(err)
    })
  }

  removeTeamMember(eid, tid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `DELETE FROM Employee_Team WHERE Employee_Id=@eid AND Team_Id=@tid`
      this.dbs.db.query(sql, { eid: eid, tid: tid }).then(() => {
        ok()
      }).catch(err)
    })
  }


  getProjectTeam(pid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `SELECT * FROM Team WHERE Project_Id=@pid`
      this.dbs.Team.query(sql, { pid: pid }).then((res) => {
        if (res.length != 1) {
          err()
        } else
          ok(res[0])
      }).catch(err)
    })
  }

  getTeamMembers(tid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `SELECT * FROM Employee WHERE Employee_Id IN (
        SELECT Employee_Id FROM Employee_Team WHERE  Team_Id=@tid)`
      this.dbs.db.query(sql, { tid: tid }).then((res) => {
        ok(res)
      }).catch(err)
    })
  }


  getAllOfficess(): Promise<any> {
    let sql = `SELECT * FROM Office`
    return this.dbs.Office.query(sql)
  }



}
