import { Injectable } from '@angular/core';
import { DbService } from './db.service';

@Injectable()
export class EmployeeService {

  constructor(private dbs: DbService) { }

  
  createEmptyEmployee(): any {
    return this.dbs.Employee({
      Name: '',
      Photo: '',
      Email: '',
      Access_Level: 0
    })
  }

  getAllEmployees(): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Employee'
      this.dbs.Employee.query(sql).then((res) => {
        ok(res)
      }).catch((err) => {
        console.log(err);
      })
    })
  }


  getEmployeeById(eid): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Employee WHERE Employee_Id=@eid'
      this.dbs.Employee.query(sql, { eid: eid }).then((res) => {
        ok(res)
      }).catch((err) => {
        console.log(err);
      })
    })
  }

  deleteAccount(eid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = 'DELETE  FROM Employee WHERE Employee_Id=@eid'
      this.dbs.Employee.query(sql, { eid: eid }).then((res) => {
        ok()
      }).catch(err)
    })
  }

}
