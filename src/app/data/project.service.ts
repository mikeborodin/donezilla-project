import { Injectable } from '@angular/core';
import { DbService } from './db.service';
import { ProjectStatus } from '../domain/ProjectStatus';

@Injectable()
export class ProjectService {


  constructor(private dbs: DbService) { }



  getAllProjects(): Promise<Array<any>> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Project'
      this.dbs.db.query(sql).then((res) => {
        ok(res)
      }).catch(err)
    })
  }

  getProjectStatus(p): string {
    return ProjectStatus[p.Status]
  }




  loadProject(id): Promise<any> {
    return new Promise((ok, err) => {
      let sql = 'SELECT * FROM Project WHERE Project_Id=@id'
      this.dbs.Project.query(sql, { id: id }).then((res) => {
        if (res.length == 1) {
          ok(res[0])
        } else {
          err('Project not found')
        }
      }).catch(err)
    })
  }


  getRequiredSkills(pId): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `SELECT R.Project_Id, R.Skill_Id, R.Date_Start, R.Date_End, S.Title as Skill_Title FROM Required_Skills R
      JOIN Skill S ON R.Skill_Id = S.Skill_Id
      WHERE Project_Id=@id`
      this.dbs.Project.query(sql, { id: pId }).then((res) => {
        console.log(res);
        ok(res)
      }).catch(err)
    })
  }

  addRequiredSkill(pid, sid, start, end): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `INSERT INTO Required_Skills(Project_Id, Skill_Id, Date_Start, Date_End)
      VALUES (@pid, @sid, @start, @end)`
      this.dbs.db.query(sql, { pid: pid, sid: sid, start: start, end: end }).then(() => {
        ok()
      }).catch(err)
    })
  }

  removeRequiredSkill(pid, sid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = 'DELETE FROM Required_Skills WHERE Project_Id=@pid AND Skill_Id=@sid'
      console.log(`Service`, pid, sid);
      this.dbs.db.query(sql, { pid: pid, sid: sid }).then((res) => {
        ok(res)
      }).catch(err)
    })
  }

  removeProject(pid): Promise<any> {
    return new Promise((ok, err) => {
      let sql = `
      DELETE FROM Team WHERE Project_Id=@pid;
      DELETE FROM Team WHERE Project_Id=@pid;
      DELETE FROM Required_Skills WHERE Project_Id=@pid;
      DELETE FROM Project WHERE Project_Id=@pid;`
      this.dbs.db.query(sql, { pid: pid }).then(() => {
        ok()
      }).catch(err)
    })
  }

  addProject(): Promise<any> {
    return new Promise((ok, err) => {

      let p = this.dbs.Project({
        Status: 0,
        Title: 'New Porject',
        Description: 'Description...',
        Start_Date: new Date(),
        End_Date: new Date(),
      })

      p.save().then(() => {
        let t = this.dbs.Team({
          Title: `Team Title`,
          Project_Id: p.identity()
        })

        t.save().then(() => {
          ok(p.identity())
        }).catch(console.error);

      }).catch(err)
    })
  }

  getProjectCost(pid): Promise<any> {
    let sql = `SELECT SUM(DATEDIFF(hh,Start_Date, End_Date)*10) FROM Task WHERE Project_Id=@pid`
    return this.dbs.db.query(sql, { pid: pid })
  }
}
