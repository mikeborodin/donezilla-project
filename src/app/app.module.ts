import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatToolbarModule, MatButtonModule, MatCheckboxModule, MatRadioModule,
  MatIconModule, MatCardModule, MatTabsModule, MatDialogModule, MatInputModule,
  MatGridListModule, MatListModule, MatSlideToggleModule, MatProgressSpinnerModule,
  MatProgressBarModule, MatOptionModule, MatSelectModule, MatSnackBarModule,
  MatTooltipModule, MatMenuModule, MatDatepickerModule, MatNativeDateModule, MatTableModule, MatPaginatorModule,
} from '@angular/material';
import { NgxElectronModule } from 'ngx-electron';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
//SwiperModule,
import { NouisliderModule } from 'ng2-nouislider';
import { DragScrollModule } from 'ngx-drag-scroll';

//Components
import { AppComponent } from './ui/app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormatSecondsPipe } from './ui/pipes/format-seconds.pipe';
import { TruncatePipe } from './ui/pipes/truncate.pipe';
import { LoginComponent } from './ui/login/login.component';
import { DashboardComponent } from './ui/dashboard/dashboard.component';

import { AuthService } from './data/auth.service';
import { DbService } from './data/db.service';
import { HelpComponent } from './ui/help/help.component';
import { AccountComponent } from './ui/account/account.component';
import { ProjectComponent } from './ui/project/project.component';
import { EmployeesComponent } from './ui/employees/employees.component';
import { TasksComponent } from './ui/tasks/tasks.component';
import { CustomersComponent } from './ui/customers/customers.component';
import { ContractsComponent } from './ui/contracts/contracts.component';
import { AuthGuard } from './ui/auth.guard';
import { ProjectService } from './data/project.service';
import { ProjectDetailComponent } from './ui/project/project-detail/project-detail.component';
import { TeamService } from './data/team.service';
import { EmployeeService } from './data/employee.service';
import { EditDialogComponent } from './ui/employees/edit-dialog/edit-dialog.component';
import { CustomerService } from './data/customer.service';
import { CustomerEditDialogComponent } from './ui/customers/customer-edit-dialog/customer-edit-dialog.component';
import { TaskService } from './data/task.service';

//canActivate: [AuthGuard],
const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'projects', pathMatch: 'full' },
      { path: 'projects', component: ProjectComponent, data: { title: 'Проекти' } },
      { path: 'projects/:id', component: ProjectDetailComponent, data: { title: 'Деталі проекту' } },

      { path: 'employees', component: EmployeesComponent, data: { title: 'Працівники' } },
      { path: 'tasks', component: TasksComponent, data: { title: 'Завдання' } },
      { path: 'contracts', component: ContractsComponent, data: { title: 'Контракти' } },
      { path: 'customers', component: CustomersComponent, data: { title: 'Замовники' } },

      { path: 'help', component: HelpComponent, data: { title: 'Довідка' } },
      { path: 'account', component: AccountComponent, data: { title: 'Налаштування аккаунта' } },
    ]
  },
  // {
  //   path: 'adminpages', component: AdminComponent, children: [
  //     { path: '', redirectTo: 'master', pathMatch: 'full' },
  //     { path: 'master', component: MasterComponent },
  //     { path: ':id/:section', component: DetailComponent },
  //   ],
  // },
];

@NgModule({
  declarations: [
    AppComponent,
    FormatSecondsPipe,
    TruncatePipe,
    LoginComponent,
    DashboardComponent,
    HelpComponent,
    AccountComponent,
    ProjectComponent,
    TasksComponent,
    EmployeesComponent,
    CustomersComponent,
    CustomerEditDialogComponent,
    ContractsComponent,
    ProjectDetailComponent,
    EditDialogComponent
  ],
  entryComponents: [EditDialogComponent, CustomerEditDialogComponent],
  imports: [
    BrowserModule,
    FormsModule,
    NgxElectronModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    DragScrollModule,
    NouisliderModule,
    //SwiperModule,
    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatDialogModule,
    MatInputModule,
    MatGridListModule,
    MatListModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatOptionModule,
    MatSelectModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
  ],

  providers: [
    AuthGuard,
    AuthService,
    DbService,
    ProjectService,
    TeamService,
    EmployeeService,
    CustomerService,
    TaskService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
